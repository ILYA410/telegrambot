﻿using System;
using System.Linq;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace TelegramBot
{
	public class Bot
	{
		private Thread _mainThreadBot;

		public void StartBot()
		{
			if (_mainThreadBot == null || !_mainThreadBot.IsAlive)
			{
				_mainThreadBot = new Thread(DoWork) { IsBackground = true };
				_mainThreadBot.Start();
				Console.WriteLine("Бот запущен");
			}
			else
			{
				// TODO: Бот уже запущен (писать логи или выводить сообщение)
				Console.WriteLine("Бот уже запущен");
			}
		}

		public void StopBot()
		{
			if (_mainThreadBot != null && _mainThreadBot.IsAlive)
			{
				_mainThreadBot.Abort();
				Console.WriteLine("Бот остановлен");
				// TODO: Бот остановлен (писать логи или выводить сообщение)
			}
		}

		private static async void DoWork()
		{
			int lastUpdateId = 0;
			const string token = "237472681:AAHL8qZE_x70DV76e0x9zuIFPlLKLGfjbn4";

			while (true)
			{
				Thread.Sleep(1000);

				WebAction webAction = new WebAction(token);
				JObject jObject = await webAction.GetUpdates(lastUpdateId);

				JToken jToken = jObject["result"];
				if (!jToken.Any())
					continue;

				// TODO: Паралельный цикл?
				foreach (JToken pair in jToken)
				{
					lastUpdateId = int.Parse(pair["update_id"].ToString());
					int chatId = int.Parse(pair["message"]["chat"]["id"].ToString());

					if (pair["message"]["text"] != null)
					{
						string from = pair["message"]["from"].ToString().Replace("\r\n", "").Replace(" ", "");
						string text = pair["message"]["text"].ToString();
						Console.WriteLine(DateTime.Now + "\t" + from + ": " + text);

						await webAction.SendMessage(chatId, $"<b>{text}</b>", "HTML");
						//await webAction.SendPhoto(chatId);

						// TODO: Обрабатывать команды и т.д
					}
					else if (pair["message"]["document"] != null)
					{
						// TODO: Обработка входящего файла
						string fileName = pair["message"]["document"]["file_name"].ToString();
						string fileId = pair["message"]["document"]["file_id"].ToString();

						await webAction.GetFile(fileId, fileName);
					}
					else if (pair["message"]["document"] != null)
					{
						// TODO: Обработка входящего файла
						string fileName = pair["message"]["document"]["file_name"].ToString();
						int fileSize = int.Parse(pair["message"]["document"]["file_size"].ToString());
						string fileId = pair["message"]["document"]["file_id"].ToString();
					}
				}
				lastUpdateId++;
			}
		}
	}
}