﻿using System;

//https://github.com/MrRoundRobin/telegram.bot
namespace TelegramBot
{
	internal class Program
	{
		private static void Main()
		{
			Bot bot = new Bot();
			bot.StartBot();
			while (true)
			{
				switch (Console.ReadLine())
				{
					case "start":
						bot.StartBot();
						break;
					case "stop":
						bot.StopBot();
						break;
					case "exit":
						return;
				}
			}
		}
	}
}