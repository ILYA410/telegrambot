﻿using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace TelegramBot
{
	public class WebAction
	{
		private const string BaseUrl = "https://api.telegram.org/bot";
		private readonly string _token;
		private readonly string _url;

		public WebAction(string token)
		{
			_token = token;
			_url = BaseUrl + _token;
		}

		public async Task<JObject> GetUpdates(int lastUpdateId)
		{
			string url = _url + "/getUpdates?offset=" + lastUpdateId;
			using (HttpClient httpClient = new HttpClient())
				return JObject.Parse(await httpClient.GetStringAsync(url));
		}

		public async Task SendMessage(int chatId, string text, string parseMode = "", bool disableNotification = true)
		{
			JObject json = new JObject
			{
				["chat_id"] = chatId,
				["text"] = text,
				["parse_mode"] = parseMode,
				["disable_notification"] = disableNotification
			};
			string serializeObject = json.ToString().Replace("\r\n", "").Replace(" ", "");
			StringContent stringContent = new StringContent(serializeObject, Encoding.UTF8, "application/json");
			string url = _url + "/sendMessage";
			using (HttpClient httpClient = new HttpClient())
				await httpClient.PostAsync(url, stringContent);
		}

		public async Task SendPhoto(int chatId, string filePath = @"C:\Work\IMG_17102014_011612.png", string caption = "", bool disableNotification = true)
		{
			using (MultipartFormDataContent multipartFormDataContent = new MultipartFormDataContent())
			{
				multipartFormDataContent.Add(new StringContent(chatId.ToString(), Encoding.UTF8), "chat_id");
				multipartFormDataContent.Add(new StringContent(caption, Encoding.UTF8), "caption");
				multipartFormDataContent.Add(new StringContent(disableNotification.ToString(), Encoding.UTF8), "disable_notification");
				using (FileStream fileStream = new FileStream(filePath, FileMode.Open))
				{
					multipartFormDataContent.Add(new StreamContent(fileStream), "photo", "IMG_17102014_011612.png");
					string url = _url + "/sendPhoto";
					using (HttpClient httpClient = new HttpClient())
						await httpClient.PostAsync(url, multipartFormDataContent);
				}
			}
		}

		public async Task GetFile(string fileId, string fileName)
		{
			JObject json = new JObject { ["file_id"] = fileId };
			string serializeObject = json.ToString().Replace("\r\n", "").Replace(" ", "");
			StringContent stringContent = new StringContent(serializeObject, Encoding.UTF8, "application/json");

			string jsonRequest = _url + "/getFile";

			using (HttpClient httpClient = new HttpClient())
			{
				HttpResponseMessage httpResponseMessage = await httpClient.PostAsync(jsonRequest, stringContent);
				string jsonResponse = await httpResponseMessage.Content.ReadAsStringAsync();

				JObject jObject = JObject.Parse(jsonResponse);
				string filePath = jObject["result"]["file_path"].ToString();
				int fileSize = int.Parse(jObject["result"]["file_size"].ToString());
				string uri = "https://api.telegram.org/file/bot" + $"{_token}/{filePath}";

				byte[] buffer = new byte[fileSize];
				using (Stream stream = await httpClient.GetStreamAsync(uri))
					await stream.ReadAsync(buffer, 0, fileSize);
				File.WriteAllBytes(fileName, buffer);
			}
		}
	}
}